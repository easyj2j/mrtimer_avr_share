/**
 * Project developed in Atmel Studio 6.2
 *
 * F_CPU = 8MHz
 *
 * prescale values: 8, 64, 256, 1024 (#also 32 & 128 for timer2)
 * 8 MHz/1024 =    7812.5 Hz -> 128 usec/count (max time, 16 bit: 8.4 s, 8 bit: 33 ms)
 * 8 MHz/256  =   31250 Hz -> 32 usec/count
 * 8 MHz/128  =   62500 Hz -> 16 usec/count#
 * 8 MHz/64   =  125000 Hz ->  8 usec/count
 * 8 MHz/32   =  250000 Hz ->  4 usec/count#
 * 8 MHz/8    = 1000000 Hz ->  1 usec/count (max time, 16 bit: 65.5 ms, 8 bit: 256 us)
 * 8 MHz                   -> .125 usec/count
 *
 *
 * Timer2: display counter
 * Need .25 msec for display counter:
 * .25m/4u    =   62.5# -> within accuracy of clock
 * .25m/1u    =  250 -> exact, use Timer2: 8 prescale, compare to 249 (250 counts)
 *                   => use overflow instead of compare (within accuracy of clock)
 * .25m/.125u = 2000 -> exact but too high for 8 bit timers
 *
 *
 * Timer1: LED/SPK (delay) start timer
 * Need LED random delay time between 2 - 8 s:
 * => use 16 bit Timer1 w 1024 prescale (128 usec/count)
 * For random, read Timer2 (TCNT2): 1-255 counts
 *  (effectively random since Timer2
 *   counts continuously from 0 - 255 @~1MHz)
 *
 * Move to variable min/max delay times (in equal increments), min: 1 s, max: 9 s
 * 1/128 usec = 7812.5 Hz
 * => delay = OCR1A = ((Longest - Shortest) * TCNT2/255 + Shortest) * 7812.5 == ledOnTime()
 *    eg. 2 s - 8 s: ((8 - 2) * TCNT2/255 + 2) * 7812.5
 * Max delay is limited to 8.4 s -> ok for now, offer user 1 - 8.
 *
 *
 * Timer0: LED/SPK (pulse width) stop timer
 * LED pulse time - need about 80 msec:
 * OVF = 256 * 128 us/count = 33 msec
 * => Use OVF flag to toggle bool, stop LED after 1 bool cycle = 66 msec
 *
 *
 * Buzzer
 * Buzzer + connected to PC3, pin 26.
 *
 *
 * Competition Mode
 * Uses PORTC2 ...
 *
 */

/*
 * Ports in use:
 * B0 - b
 * B1 - g
 * B2 - c
 * B3 - f
 * B4 - d
 * B5 - e
 * B6 - xtal1
 * B7 - xtal2
 * C0 - DIGITS[0] - LSD, rightmost facing display, D4 on schematic
 * C1 - led
 * C2 - competition mode: out (master) - in (slave)
 * C3 - buzzer
 * C4 - dp
 * C5 - nc (header)
 * C6 - reset
 * D0 - rxd (ftdi tx)
 * D1 - txd (ftdi rx)
 * D2 - motion switch (header)
 * D3 - stop switch (header)
 * D4 - a
 * D5 - DIGITS[1]
 * D6 - DIGITS[2]
 * D7 - DIGITS[3] - MSD, leftmost facing display, D1 on schematic
*/

#define REG_CAST volatile uint8_t*
/*
From iom328p.h & sfr_defs.h:

#define PORTC _SFR_IO8(0x08)
#define _SFR_IO8(io_addr) ((io_addr) + __SFR_OFFSET)
#define __SFR_OFFSET 0x20
   => PORTC == 0x08 + 0x20 = 0x28 == 8-bit memory address

uint8_t DIGIT_PORTS[NUMBER_OF_DIGITS] = {PORTC,PORTD,PORTD,PORTD};

DIGIT_PORTS[currentDigit] returns the port (uint8_t) corresponding to currentDigit (0-3)

(REG_CAST)DIGIT_PORTS[currentDigit] casts the uint8_t to a pointer to a uint8_t

*(REG_CAST)DIGIT_PORTS[currentDigit] returns the value at the corresponding port

-----------------
*/

/*
  friend ISR(INT0_vect);
  friend void INT0_vect(void);
  friend void _VECTOR(1)(void);
  
  The following fields are public because they are accessed in
  the ISR routines & am unable to make the ISRs friend funcs
  so that they can access the members as private.
*/
// Flags set by ISRs.
static bool interrupt0, interrupt1, pcint1, timer0_ovf, timer1_compa, timer2_ovf, usart_rx;
        
/* 
v1.5
No longer use defines; now specified as SEVEN_SEGMENT[10] array of uint8_t.
Still useful as reference.
a-g represent the 7-segments & decimal point (see datasheet for ATA8041AB).
Each segment is one pin of PORTB: PORTB7654 PORTB3210 = faed 'dp'cgb
#define SEV_SEG_ZERO    0xF5  // a-f on    = 1111 0101
#define SEV_SEG_ONE     0x05  // b,c       = 0000 0101
#define SEV_SEG_TWO     0x73  // a,b,d,e,g = 0111 0011
#define SEV_SEG_THREE   0x57  // a-d,g     = 0101 0111
#define SEV_SEG_FOUR    0x87  // b,c,f,g   = 1000 0111
#define SEV_SEG_FIVE    0xD6  // a,c,d,f,g = 1101 0110
#define SEV_SEG_SIX     0xF6  // a,c-g     = 1111 0110
#define SEV_SEG_SEVEN   0x45  // a-c       = 0100 0101
#define SEV_SEG_EIGHT   0xF7  // a-g       = 1111 0111
#define SEV_SEG_NINE    0xC7  // a-c,f,g   = 1100 0111

v3.0
Use crystal/resonator osc (fuse CKSEL3...1 = 011), which requires PORTB6 & 7 for XTAL.
When the crystal osc is selected (fuse CKSEL3...1 = 011), 
  assume writing to XTAL pins is a NOP.  Check out!
If so -> no need to change the above; just need to write segm's f & a separately.
Since dp is always 0, if exchange dp with 'f' -> only have to write to one pin ('a') separately. 

Because of how this affects board layout, make these changes:
dp: PB3 (17) -> PC4 (27) - connects to display pin3
f:  PB7 (10) -> PB3 (17) - connects to display pin10
a:  PB6  (9) -> PD4  (6) - connects to display pin11

So array now represents: PORTD4 PORTB54 PORTB3210 = a ed fcgb
#define SEV_SEG_ZERO    0x7D  // a-f on    =  1 11 1101
#define SEV_SEG_ONE     0x05  // b,c       =  0 00 0101
#define SEV_SEG_TWO     0x73  // a,b,d,e,g =  1 11 0011
#define SEV_SEG_THREE   0x57  // a-d,g     =  1 01 0111
#define SEV_SEG_FOUR    0x0F  // b,c,f,g   =  0 00 1111
#define SEV_SEG_FIVE    0x5E  // a,c,d,f,g =  1 01 1110
#define SEV_SEG_SIX     0x7E  // a,c-g     =  1 11 1110
#define SEV_SEG_SEVEN   0x45  // a-c       =  1 00 0101
#define SEV_SEG_EIGHT   0x7F  // a-g       =  1 11 1111
#define SEV_SEG_NINE    0x4F  // a-c,f,g   =  1 00 1111
*/

/********************** Function prototypes *************************/
/*
void MotionSwitch_psr();
void StopSwitch_psr();
void CompModeSlaveTrigger_psr();
void LedSpkOff_psr();
void TriggerStart_psr();
void DisplayCounter_psr();
void USART_RX_psr();

//inline uint16_t ledOnTime(byte delayMin, byte delayMax);
//void setCompetitionMode(CompetitionMode mode);

//void setupSerial();

//// Don't write if data == current eeprom value.
//void eepromUpdate(uint16_t address, byte data);
//byte eepromRead(uint16_t address);

void PrintTime();
void PrintTimes();
void putnum(uint16_t num);
void printnumln(uint16_t num);
//void putch(char ch);
void print(char* str);
void println(char* str);
void OutputTime();
*/
#endif /* MOTION_REACTION_TIMER_H_ */