/*
 * Utilities.cpp
 *
 * Created: 9/12/2014 3:30:08 PM
 *  Author: John
 */ 

#include "MRTimer.h"

namespace MRTimer
{
    void Timer::putch(char ch)
    {
        serialPort.Write(ch);
    }

    /*
    // 0 <= num <= 65535
    void putnum(int num)
    {
        if(num <= 9) putch(num + 48);
        else
        {
            putnum(num / 10);
            putnum(num % 10);
        }
    }
    */

    // 0 <= uint16_t <= 65535 but force output to 0000 <= num <= 9999
    // Outputs a char corresponding to each digit of an uint16_t.
    void Timer::putnum(uint16_t num)
    {
        if(num > 9999)              // rare - generally timer just running
        {
            putnum(9999);
            /* The following does the same but requires 24/28 (rel/debug) bytes more in flash.
               Why?  Would think it required less. */
            //putch(57);  putch(57);  putch(57);  putch(57);   // '9'
            
        }
        else if(num > 999)          // 4 digits
        {
            putch(  num / 1000              + 48);
            putch(( num % 1000) / 100       + 48);
            putch(((num % 1000) % 100) / 10 + 48);
            putch(((num % 1000) % 100) % 10 + 48);
        }
        else if(num > 99)           // 3 digits - generally expect this
        {
            putch(48);             // '0'
            putch( num / 100       + 48);
            putch((num % 100) / 10 + 48);
            putch((num % 100) % 10 + 48);
        }
        else if(num > 9)            // 2 digits - maybe by very fast person - likely short cutted
        {
            putch(48);
            putch(48);
            putch(num / 10 + 48);
            putch(num % 10 + 48);
        }
        else                        // 1 digit - mistake somewhere
        {
            putch(48);
            putch(48);
            putch(48);
            putch(num + 48);
        }
    }

    void Timer::printnumln(uint16_t num)
    {
        putnum(num);
        putch('\n');
    }

    void Timer::print(const char* str)
    {
        for(int i = 0; str[i] != '\0'; i++)
        {
            putch(str[i]);
        }
    }

    void Timer::println(const char* str)
    {
        print(str);
        putch('\n');
    }
    
    void Timer::OutputFirmwareVersion()
    {
        print(FIRMWARE_VERSION);
    }

    // rtIndex valid only when OutputTime() called in StopSwitch_psr().
    void Timer::OutputTime()
    {
        if(!wasAppNotified)
        {
          // Output '!' to match case where motion switch is pressed (see INT0_psr).
          // Now stream received by app will be identical (12 bytes/score).
          putch(ZERO_APP_DISPLAY);
        }
        wasAppNotified = false;
    
        // Output exactly 4 digits for both reaction & response times.
        // Output total (besides ZERO_APP_DISPLAY char) = 11 bytes.
        putch('&');
        putnum(responseTimes[rtIndex].reaction);
        putch('&');
        putnum(responseTimes[rtIndex].response);
        putch('\n');
    }
    
#ifdef _DEBUG_  // Reduce RAM usage.
    void Timer::PrintHeader()
    {
        println("Motion and Reaction Timer");
        println("Response time = Reaction time + Motion time");
        println("(all times in ms)");
        println("");
        println("-------------------------------------------");
    }

    // TODO: get rid of PrintTime.
    // rtIndex valid only when PrintTime() called in StopSwitch_psr().    
    void Timer::PrintTime()
    {
        if(responseTimes[rtIndex].reaction)
        {
            print("Reaction time: ");
            printnumln(responseTimes[rtIndex].reaction);
            print("Motion time:   ");
            printnumln(responseTimes[rtIndex].response - responseTimes[rtIndex].reaction);
        }
        print("Response time: ");
        printnumln(responseTimes[rtIndex].response);
        println("----------");
    }

    // TODO: loop around end.
    void Timer::PrintTimes()
    {
        if(!responseTimes[0].response) { println("No times to print."); }
        else
        {
            putch('\n');
            println("Last 10 times (latest on top)");
            println("Reaction + Motion = Response");
            for(int i = rtIndex - 1; i >= 0; i-- )
            {
                putnum(responseTimes[i].reaction);
                print(" + ");
                putnum(responseTimes[i].response - responseTimes[i].reaction);
                print(" = ");
                printnumln(responseTimes[i].response);
            }
            println("------------------------------");
        }
    }
#endif // _DEBUG_
}