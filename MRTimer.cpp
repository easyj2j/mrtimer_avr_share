/**
 * MRTimer.cpp
 *
 * v3.1: Uses external clock (crystal) instead of internal rc clock.
 *
 * Created: 1/28/2015
 * Author: J Mac
 */ 

/**
 * ATmega328p -> ATA8041AB quad 7 segment display (QSSD) from SparkFun.
 * Uses internal timers to count & write to QSSD.
 * Measures reaction and response times up to 65 seconds (display to 9.999).
 * Uses random delay after which an LED and/or speaker is pulsed to signal start of timer.
 * Response time = Reaction time + Motion time.
 * Uses Motion switch (INT0) to record intermediate (reaction) time.
 * Uses Stop switch (INT1) to record response time.
 * Motion time derived from reaction and response times.
 * Last 10 times stored in ring buffer.
 * Option to output times to Serial/Bluetooth.
 * Can control program thru Serial/Bluetooth:
 *   output times
 *   min/max delay times
 *   trigger source: LED, speaker, both
 *   competition mode: off, master, slave
 * Ability to upload application through Bluetooth (USB) connection.
 * (Hold Motion switch closed and cycle power. Must cycle power afterwards.)
 */

//#include <avr/io.h>
#include "MRTimer.h"

int main(void)
{
    MRTimer::Timer t;
    t.Start();
}

namespace MRTimer
{
    using namespace Atmega328p;
        
    // Initialize statics
    bool Timer::isBasicResponse;
    bool Timer::interrupt0;
    bool Timer::interrupt1;
    bool Timer::pcint1;
    bool Timer::timer0_ovf;
    bool Timer::timer1_compa;
    bool Timer::timer2_compa;
    bool Timer::timer2_ovf;
    bool Timer::usart_rx;
    
    IOPin Timer::competitionTrigger{&PORTC, PORTC2};   // ATmega328 pin 25
    InternalTimer Timer::triggerDelayTimer{1};
    ExternalInterrupt Timer::stopSwitchInterrupt{1, InterruptType::FALLING_EDGE};
    
    const uint8_t Timer::SEVEN_SEGMENT[10] = {0x7D,0x05,0x73,0x57,0x0F,0x5E,0x7E,0x45,0x7F,0x4F};
    
    // ATmega328 pins 23, 11-13
    const uint8_t Timer::DIGITS[NUMBER_OF_DIGITS] = {PORTC0,PORTD5,PORTD6,PORTD7};  // LSD -> MSD
        
    // {0x28,0x2B,0x2B,0x2B}
    const volatile uint8_t* Timer::DIGIT_PORTS[NUMBER_OF_DIGITS] = {&PORTC,&PORTD,&PORTD,&PORTD};
    
    Timer::Timer()
    {
        InitializeMCU();

        rtIndex = 0;
        
        receivedChar = '\n';
        previousChar = '*';
        
        SetDelayFromEeprom();

        digit0Counter = digit1Counter = digit2Counter = digit3Counter = 0;  // zero display
        latch = true;   // freeze counter display @ zero
        msCounter = 0;
        
        timer0DividedBy2 = timingState = wasAppNotified = false;
        printTime = outputTime = false;
        
    #ifdef _DEBUG_
        printTime = true;
        PrintHeader();
    #endif // _DEBUG_
    
        // For testing
        //trigger = Trigger::BUZZER;
    }

    void Timer::Start()
    {
        // Globally enable interrupts.
        sei();

        // Poll for flags set by ISRs.
    #pragma region    
        for(;;)
        {
            if(timer2_compa)
            {
                timer2_compa = false;
                DisplayCounter_psr();
            }
            if(timer1_compa)
            {
                timer1_compa = false;
                TriggerStart_psr();
            }
            if(timer0_ovf)
            {
                timer0_ovf = false;
                
                // Call once for every 2 interrupts (2 overflow cycles).
                if(timer0DividedBy2) { TriggerStop_psr(); }
                
                timer0DividedBy2 = !timer0DividedBy2;
            }
            if(interrupt0)
            {
                interrupt0 = false;
                IS_BASIC_MODEL ? MotionSwitch_Basic_psr() : MotionSwitch_psr();
            }
            if(interrupt1)
            {
                interrupt1 = false;
                StopSwitch_psr();
            }
            if(pcint1)
            {
                pcint1 = false;
                CompModeSlaveTrigger_psr();
            }
            if(usart_rx)
            {
                receivedChar = serialPort.Read();
                usart_rx = false;
                USART_RX_psr();
            }
        }
    #pragma endregion
    }

    /*
     * Write 1 digit after every timer2 compare match (every .25 ms).
     * Rotate thru all 4 digits starting w LSD (right-most).
     * Display digits: 3 2 1 0 -> digit3Counter digit2Counter digit1Counter digit0Counter
     * Note: Timer2 never stops counting, just the display is frozen on latch = true.
     */
    // Continuous counter; sequentially writes to one multiplexed digit every .25 ms.
    ISR(TIMER2_COMPA_vect)
    {
        Timer::timer2_compa = true;
    }

    /**
     * Timer1 turns on LED and/or speaker after random delay time.
     * Random value read from Timer2, which counts continuously from 0-255 @1MHz.
     * Random value modified and set to output compare reg A.
     *
     * TIMER1_COMPA_vect:
     *  self-disable
     *  in competition mode, send external trigger to slave
     *  turn on LED/speaker
     *  zero & latch display
     *  start Timer0 (LED off timer)
     *  re-enable INT's (MOTION/STOP)
     */
    // Turn on LED/speaker after random delay between 1 - 8 seconds.
    ISR(TIMER1_COMPA_vect)
    {
        // Self-disable & clear.
        Timer::triggerDelayTimer.Stop();
        Timer::triggerDelayTimer.Reset();

        Timer::timer1_compa = true;
    }

    /**
        * Timer0 turns off LED and/or speaker after pulse time.
        *
        * TIMER0_COMPA_vect:
        *  self-disable & clear
        *  turn off LED/speaker
        */
    // Turn off LED/speaker after pulse time of 66 ms (2 overflow cycles).
    ISR(TIMER0_OVF_vect)
    {
        Timer::timer0_ovf = true;
    }
   
    /*
     * Don't care about switch bounce or multiple pressings by user:
     *  1) If LED hasn't fired, we simply reset the delay timer. The display
     *     is frozen at zero and the delay is random as expected.
     *     We also send a char to phone app so it can zero its display.
     *  2) If LED has fired, then the display is counting.  We log the time
     *     of the first release (press) and ignore all others including any bouncing.
     */
    // External interrupt 0 (motionSwitch - any level change).
    ISR(INT0_vect)
    {
        Timer::interrupt0 = true;
    }

    // External interrupt 1 (STOP_SWITCH_PIN HI -> LO).
    ISR(INT1_vect)
    {
        // Self-disable
        Timer::stopSwitchInterrupt.Disable();

        Timer::interrupt1 = true;
    }

    // Pin change interrupt 1 (Competition mode slave trigger) - any level change.
    ISR(PCINT1_vect)
    {
        // Only set on hi -> low toggle.
        if( !Timer::competitionTrigger.IsHigh() )
        {
            Timer::pcint1 = true;
        }
    }

    // Receives control commands through USART.
    ISR(USART_RX_vect)
    {
        Timer::usart_rx = true;
    }

    void Timer::InitializeMCU()
    {
        // In case of errant code, clear watchdog reset flag then disable watchdog.
        // This is suggested by datasheet. Is it necessary?
        MCUSR = 0;
        wdt_disable();

        // Set unconnected pins to input w pullup.
        IOPin{&PORTC, PORTC5}.SetInputHigh();
        
        // The following pins are unused only in BASIC_MODEL.
        if(IS_BASIC_MODEL)
        {
            IOPin{&PORTC, PORTC2}.SetInputHigh();   // Competition switch
            IOPin{&PORTD, PORTD0}.SetInputHigh();   // BT RXD
            IOPin{&PORTD, PORTD1}.SetInputHigh();   // BT TXD
        }
        
        if(!IS_BASIC_MODEL) { serialPort.Enable(); }
    
        // Set Competition mode off.
        SetCompetitionMode(CompetitionMode::OFF);
    
        // Set internal pullup on stopSwitch.  motionSwitch has external pullup.
        stopSwitch.SetInputHigh();

        // Setup external interrupt for motionSwitch (INT0)
        //motionSwitchInterrupt{0, InterruptType::LogicChange};
        motionSwitchInterrupt.Enable();
        motionSwitchInterrupt.Clear();

        // Setup external interrupt for STOP_SWITCH_PIN (INT1)
        stopSwitchInterrupt.Enable();
        stopSwitchInterrupt.Clear();

        // Set LED and speaker pins to output low.
        triggerLed.SetOutputLow();
        // Basic model has no buzzer (unconnected).
        IS_BASIC_MODEL ? triggerBzr.SetInputHigh() : triggerBzr.SetOutputLow();
 
        // Set QSSD digit pins as output. DDRx = PORTx - 1
        for(uint8_t i = 0; i < Timer::NUMBER_OF_DIGITS; i++)
        {
            *(REG_CAST)(DIGIT_PORTS[i] - 1) |= _BV(DIGITS[i]);
        }

        // Set QSSD segment pins as output.
        *segmentsPort.pDDR = 0xFF;
        segmentA.SetOutputLow();
        decimalPoint.SetOutputLow();

        // Setup display counter/timer
        displayTimer.EnableInterrupt(InterruptState::COMPARE_MATCH_A);
        uint8_t compareValue = 249; // want to call single byte overload
        displayTimer.SetOutputCompareAValue(compareValue);
        displayTimer.Start();   // STARTS HERE & NEVER STOPS

        // Read state of motionSwitch.
        isBasicResponse = motionSwitch.IsHigh();

        if(!isBasicResponse) return;    // motion only - triggers not used

        // Setup LED delay -> pulse on counter
        triggerDelayTimer.EnableInterrupt(InterruptState::COMPARE_MATCH_A);
        triggerDelayTimer.SetOutputCompareAValue(LedOnTime(delayMin, delayMax));
        triggerDelayTimer.Start();

        // Setup LED/SPK pulse off timer
        triggerPulseTimer.EnableInterrupt(InterruptState::OVERFLOW);
    }

    // TODO: This hasn't been tested thoroughly on device.
    void Timer::SetCompetitionMode(CompetitionMode mode)
    {
        compMode = mode;
        switch(mode)
        {
            case CompetitionMode::OFF:
                competitionTrigger.SetInputHigh();
                break;
            case CompetitionMode::MASTER:
                competitionTrigger.SetOutputHigh();
                break;
            case CompetitionMode::SLAVE:
                competitionTrigger.SetInputHigh();
                // Enable interrupt & clear int & set flags.
                competitionInterrupt.Enable();
                competitionInterrupt.Clear();
                pcint1 = false;
                // Disable delay timer & clear int & set flags.
                triggerDelayTimer.Stop();
                triggerDelayTimer.ClearOutputCompareAMatchFlag();
                timer1_compa = false;
                // Turn off LED/speaker.
                TriggerStop_psr();       
                return;
        }
        // Disable interrupt & clear int & set flags.
        competitionInterrupt.Disable();
        competitionInterrupt.Clear();
        pcint1 = false;
        // Reset & restart display and Timer1.                   
        if(!IS_BASIC_MODEL) { MotionSwitch_psr(); }
    }

    // Algorithm for setting LED/buzzer random start time.  Reads displayTimer (TCNT2).
    uint16_t Timer::LedOnTime(byte delayMin, byte delayMax)
    {
        uint8_t timerCount = displayTimer.Read();
        return (delayMin + (delayMax - delayMin) * timerCount/DISPLAYTIMER_MAXCOUNT) * DISPLAYTIMER_FREQ;
    }

    // Read stored delay settings from EEPROM.
    void Timer::SetDelayFromEeprom()
    {
        delayMin = eeprom.Read(EEPROM_ADDR_DELAY_MIN);
        if( (delayMin < TRIGGER_MIN_DELAY) || (delayMin > TRIGGER_MAX_DELAY) )
        { delayMin = 3; }
            
        delayMax = eeprom.Read(EEPROM_ADDR_DELAY_MAX);
        if( (delayMax < delayMin) || (delayMax > TRIGGER_MAX_DELAY) )
        { delayMax = 7; }
    }
}
