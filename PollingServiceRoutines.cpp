/**
 * PollingServiceRoutines.cpp
 *
 * Created: 1/28/2015
 *  Author: J Mac
 */ 

#include "MRTimer.h"

namespace MRTimer
{
    using namespace Atmega328p;
    
    bool speakerOn;
    bool motionSwitchPressed;
    bool autoTriggerEnabled = true;
    
    // Motion switch interrupt disabled for this amount of time
    // to eliminate bounce on press & release.
    uint16_t bounceEliminateTime = 3906; // 0.5 secs

    /*
     * Write 1 digit after every timer2 overflow (every .25 ms).
     * Rotate thru all 4 digits starting w LSD (right-most).
     * Display digits: 3 2 1 0 -> digit3Counter digit2Counter digit1Counter digit0Counter
     * Note: Timer2 never stops counting, just the display is frozen on latch = true.
     */
    // Timer1 continuous counter; sequentially writes to one multiplexed digit every .25 ms.
    void Timer::DisplayCounter_psr()
    {
        static uint8_t currentDigit = 0;     // which digit to write to
        static uint8_t previousDigit = 3;

        // Turn off previous digit (set digit pin HIGH).
        *(REG_CAST)DIGIT_PORTS[previousDigit] |=  _BV(DIGITS[previousDigit]);
        
        // Turn off dp.
        decimalPoint.SetLow();

        // Write segments
        if(0 == currentDigit)
        {
            if(!latch) digit0Counter++;
            if( (10 == digit0Counter) && !latch )
            {
                digit0Counter = 0;
                digit1Counter++;
            }
            *segmentsPort.pPORT = Timer::SEVEN_SEGMENT[digit0Counter];
            ((SEVEN_SEGMENT[digit0Counter] & 0x40) == 0) ? segmentA.SetLow() : segmentA.SetHigh();

            msCounter++;
        }
        else if(1 == currentDigit)
        {
            if( (10 == digit1Counter) && !latch )
            {
                digit1Counter = 0;
                digit2Counter++;
            }
            *segmentsPort.pPORT = SEVEN_SEGMENT[digit1Counter];
            ((SEVEN_SEGMENT[digit1Counter] & 0x40) == 0) ? segmentA.SetLow() : segmentA.SetHigh();
        }
        else if(2 == currentDigit)
        {
            if( (10 == digit2Counter) && !latch )
            {
                digit2Counter = 0;
                digit3Counter++;
            }
            *segmentsPort.pPORT = SEVEN_SEGMENT[digit2Counter];
            ((SEVEN_SEGMENT[digit2Counter] & 0x40) == 0) ? segmentA.SetLow() : segmentA.SetHigh();
        }
        else // 3 == currentDigit
        {
            if( (10 == digit3Counter) && !latch )
            {
                digit3Counter = 0;
            }
            // Enable decimal point
            *segmentsPort.pPORT = SEVEN_SEGMENT[digit3Counter];
            ((SEVEN_SEGMENT[digit3Counter] & 0x40) == 0) ? segmentA.SetLow() : segmentA.SetHigh();
            decimalPoint.SetHigh();
        }

        // Turn on digit (set digit pin LOW).
        *(REG_CAST)DIGIT_PORTS[currentDigit] &=  ~_BV(DIGITS[currentDigit]);

        // Switch to next digit
        previousDigit = currentDigit++;

        // If previous digit = MSD -> jump to LSD.
        if(4 == currentDigit) { currentDigit = 0; }
    }

    /**
     * Timer1 turns on LED and/or speaker after random delay time.
     * Random value read from Timer2, which counts continuously from 0-255 @1MHz.
     * Random value modified and set to output compare reg A.
     *
     * TIMER1_COMPA_vect:
     *  self-disable
     *  in competition mode, send external trigger to slave
     *  turn on LED/speaker
     *  zero & latch display
     *  start Timer0 (LED off timer)
     *  re-enable INT's (MOTION/STOP)
     */
    // Timer1 turns on LED/speaker after random delay between 1 - 8 seconds.
    // In Basic mode ...
    void Timer::TriggerStart_psr()
    {
    #ifdef _DEBUG_
        print("Delay time: ");
        printnumln(triggerDelayTimer.Read() * 0.128); // delay time in ms
    #endif // _DEBUG_

        if(IS_BASIC_MODEL)
        {
            // Clear & re-enable motionSwitchInterrupt (disabled in MotionSwitch_Basic_psr).
            motionSwitchInterrupt.Clear();
            interrupt0 = false;
            motionSwitchInterrupt.Enable();
            
            // autoTriggerEnabled == false -> motion-switch mode.
            if(!autoTriggerEnabled)
            {
                // Self-disable & clear (one-shot).
                triggerPulseTimer.Stop();
            
                return;
            }
        }

        // Set timingState state to true
        timingState = true;

        // In competition mode, send external trigger to slave.
        if(CompetitionMode::MASTER == compMode)
        {
            competitionTrigger.SetLow();
        }

        // Turn on LED/speaker
        LedSpk_TurnOnOff(true);

        latch = false;          // re-enable display
        digit0Counter = digit1Counter = digit2Counter = digit3Counter = 0;      // zero display
        msCounter = 0;          // zero counter

        // Clear any switch interrupt set flags & re-enable
        motionSwitchInterrupt.Clear();
        stopSwitchInterrupt.Clear();
        motionSwitchInterrupt.Enable();
        stopSwitchInterrupt.Enable();

        // Start LED/SPK pulse off timer
        triggerPulseTimer.Start();
    
        // In competition mode, reset external trigger on slave.
        if(CompetitionMode::MASTER == compMode)
        {
            competitionTrigger.SetHigh();
        }
    }

    /**
        * Timer0 turns off LED and/or speaker after pulse time.
        *
        * TIMER0_COMPA_vect:
        *  self-disable & clear
        *  turn off LED/speaker
        */
    // Timer0 turns off LED/speaker after pulse time of 66 ms (2 overflow cycles).
    void Timer::TriggerStop_psr()
    {
        // Self-disable & reset.
        triggerPulseTimer.Stop();
        triggerPulseTimer.Reset();

        // Turn off LED/speaker
        LedSpk_TurnOnOff(false);
    }
    
    // Turn LED/speaker on or off.
    void Timer::LedSpk_TurnOnOff(bool turnOn)
    {
        if(Trigger::LED == trigger)
        {
            turnOn ? triggerLed.SetHigh() : triggerLed.SetLow();
        }
        else
        {
            turnOn ? triggerBzr.SetHigh() : triggerBzr.SetLow();
        }
    }
    
    /*
     * Don't care about switch bounce or multiple pressings by user.
     *  On press: zero & freeze display (disable for bounceEliminateTime).
     *  On release: log time, zero & unfreeze display.
     */
    // External interrupt 0 (motionSwitch - any level change).
    void Timer::MotionSwitch_Basic_psr()
    {
        // Self-disable and clear (re-enabled in TriggerStart_psr after bounceEliminateTime).
        motionSwitchInterrupt.Disable();
        motionSwitchInterrupt.Clear();
        interrupt0 = false;
        
        // Stop, clear and reset triggerDelayTimer.
        triggerDelayTimer.Stop();
        timer1_compa = false;
        triggerDelayTimer.ClearOutputCompareAMatchFlag();
        triggerDelayTimer.Reset();
        
        // Disable auto-trigger (re-enabled in StopSwitch_psr).
        autoTriggerEnabled = false;
        
        if(!motionSwitchPressed)
        {
            // Motion switch pressed.
            motionSwitchPressed = true;

            // Zero display & latch
            latch = true;
            digit0Counter = digit1Counter = digit2Counter = digit3Counter = 0;
        
            // Set delay time.
            triggerDelayTimer.SetOutputCompareAValue(bounceEliminateTime);
            triggerDelayTimer.Start();
        }
        else
        {
            // Motion switch released.
            motionSwitchPressed = false;
        
            // Reenable display
            latch = false;
        
            // Record time (defines reaction time before / motion time after)
            responseTimes[rtIndex].reaction = 0;
            msCounter = 0;
            
            // Enable stopSwitch.
            stopSwitchInterrupt.Enable();
            
            // Reset delay time range.
            triggerDelayTimer.SetOutputCompareAValue(LedOnTime(delayMin, delayMax));
        }
    }
    
    /*
     * Don't care about switch bounce or multiple pressings by user:
     *  1) If LED hasn't fired, we simply reset the delay timer. The display
     *     is frozen at zero and the delay is random as expected.
     *     We also send a char to phone app so it can zero its display.
     *  2) If LED has fired, then the display is counting.  We log the time
     *     of the first release (press) and ignore all others including any bouncing.
     */
    // External interrupt 0 (motionSwitch - any level change).
    void Timer::MotionSwitch_psr()
    {
        int tempTime = msCounter;
        responseTimes[rtIndex].reaction = 0;

        // Reset & restart display and Timer1
        if(!timingState) // LED (TIMER1_COMPB_vect) hasn't yet fired
        {
            // Turn off Timer1.
            triggerDelayTimer.Stop();
        
            // 4/17/14: 'if' statement new - not tested thoroughly
            if(outputTime && !wasAppNotified)
            {
                // Notify phone app to zero display.
                putch(ZERO_APP_DISPLAY);
                wasAppNotified = true;
            }

            // Zero display & latch
            latch = true;
            digit0Counter = digit1Counter = digit2Counter = digit3Counter = 0;
        
            // Reset & restart Timer1 (LED delay counter)
            if(CompetitionMode::SLAVE != compMode)
            {
                triggerDelayTimer.Reset();
                triggerDelayTimer.SetOutputCompareAValue(LedOnTime(delayMin, delayMax));
                triggerDelayTimer.Start();
            }
        }
        else // LED has fired & timing has begun.
        {
            // Self-disable & clear any INT0 set flag.
            motionSwitchInterrupt.Disable();
            motionSwitchInterrupt.Clear();

            // Record time (defines reaction time before / motion time after)
            responseTimes[rtIndex].reaction = tempTime;
        }
    }

    // External interrupt 1 (STOP_SWITCH_PIN HI -> LO).
    void Timer::StopSwitch_psr()
    {
        // Stop triggered before LED fired.  If not BASIC_MODEL, reset just like Motion switch.
        if(!timingState)
        {
            if(!IS_BASIC_MODEL) { MotionSwitch_psr(); }
        }

        // Set timingState state to false
        timingState = false;

        // Latch display (loop counters digit0Counter - digit3Counter stopped)
        latch = true;

        responseTimes[rtIndex].response = msCounter;

#ifdef _DEBUG_
        // Controlled by Serial command.
        if(printTime)
        {
            PrintTime();
        }
#endif // _DEBUG_

        if(outputTime)
        {
            OutputTime();
        }

        // Store last RT_MAX_ENTRIES times.  This is only place rtIndex is changed.
        if(RT_MAX_ENTRIES == ++rtIndex) { rtIndex = 0; }
        responseTimes[rtIndex].reaction = 0;
        responseTimes[rtIndex].response = 0;

        // Get new random on-time
        triggerDelayTimer.SetOutputCompareAValue(LedOnTime(delayMin, delayMax));
    
        // Only restart timer1 (LED delay) when not in Comp mode.
        // In Comp mode, restart requires Master to press Start/Motion switch.
        if(CompetitionMode::OFF == compMode)
        {
            triggerDelayTimer.Start();
        }            

        // Clear any switch interrupt set flags & re-enable motionSwitchInterrupt.
        motionSwitchInterrupt.Clear();
        stopSwitchInterrupt.Clear();
        motionSwitchInterrupt.Enable();
        
        autoTriggerEnabled = true;
    }

    // Pin change interrupt 1 (Competition mode slave trigger) - any level change.
    // Only called on hi -> low toggle.
    void Timer::CompModeSlaveTrigger_psr()
    {
        TriggerStart_psr();
    }
    
    /*
       Handle control commands sent through USART.
       void Timer::USART_RX_psr()
       Moved to DeviceDriver.cpp
    */
}