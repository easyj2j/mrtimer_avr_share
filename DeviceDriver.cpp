/*
 * DeviceDriver.cpp
 *
 * Created: 10/17/2014 5:07:35 PM
 *  Author: John
 */ 

#include "MRTimer.h"

namespace MRTimer
{
    using namespace Atmega328p;
    
    // Handle control commands sent through USART.
    void Timer::USART_RX_psr()
    {
    #ifdef _DEBUG_
        putch(receivedChar);
    #endif // _DEBUG_
    
        static const uint8_t ASCII_ZERO = 0x30; // converts receivedChar to associated ascii number
        
        switch(receivedChar)
        {
            case 'f':           // output firmware version
                OutputFirmwareVersion();
            break;
            case 'o':           // immed send score to USART
                outputTime = true;
            break;
            case 'n':
                outputTime = false;
            break;
            case 'd':           // latch display
                latch = true;
                // Disable & reset Timer1 (LED delay timer); turn off LED.
                triggerDelayTimer.Stop();
                triggerDelayTimer.Reset();
                triggerLed.SetLow();
            break;
            case 'e':           // reenable display
                digit0Counter = digit1Counter = digit2Counter = digit3Counter = 0;
                // Get new random on time & restart timer1 (LED delay)
                if(CompetitionMode::SLAVE != compMode)
                {
                    triggerDelayTimer.SetOutputCompareAValue(LedOnTime(delayMin, delayMax));
                    triggerDelayTimer.ClearOutputCompareAMatchFlag();
                    triggerDelayTimer.Start();
                }        
                // Clear any switch interrupt set flags & re-enable motionSwitchInterrupt.
                motionSwitchInterrupt.Clear();
                stopSwitchInterrupt.Clear();
                motionSwitchInterrupt.Enable();
            break;

            case 'N':       // prelude to miN delay time
            case 'X':       // prelude to maX delay time
            case 'T':       // prelude to Trigger source
            case 'C':       // prelude to Competition mode
            case 'V':       // prelude to speaker volume (duty cycle)
            break;
            
            case '1':
            case '2':
            case '3':   // supposed to fall thru if 'if' statements don't apply
                if ('T' == previousChar)        // trigger source
                {
                    trigger = static_cast<Trigger>(receivedChar - ASCII_ZERO);
                    /*
                        Don't store/retrieve trigger, just default to LED on startup.
                        If speaker was set from Android app there's no way to turn
                        it off without the app -> could be annoying.
                        eepromUpdate(EEPROM_ADDR_TRIGGER, trigger);
                     */
                    break;
                }
                else if ('C' == previousChar)   // competition mode
                {
                    SetCompetitionMode(static_cast<CompetitionMode>(receivedChar - ASCII_ZERO));
                    break;
                }
            case '4':   // supposed to fall thru if 'if' statement doesn't apply
                if ('V' == previousChar)        // speaker volume
                {
                    //spkFreq = static_cast<SpkFreq>(receivedChar - ASCII_ZERO);
                    break;
                }
            case '5':
            case '6':
            case '7':
            case '8':
                if('N' == previousChar)      // set min led delay
                {
                    delayMin = receivedChar - ASCII_ZERO;
                    eeprom.Update(EEPROM_ADDR_DELAY_MIN, delayMin);
                }
                else if('X' == previousChar) // set max led delay
                {
                    delayMax = receivedChar - ASCII_ZERO;
                    eeprom.Update(EEPROM_ADDR_DELAY_MAX, delayMax);
                }
            break;

        #ifdef _DEBUG_
            case 'a':
                PrintTimes();  // also "#ifdef _DEBUG_" PrintTimes() definition to reduce ram usage
            break;
            case 'p':
                printTime = true;
            break;
            case 's':
                printTime = false;
            break;
        #endif // _DEBUG_

            case '\b':      // backspace
            case '\r':      // carriage return
            case '\n':      // new line
            case '0':       // blank in buffer
            case '9':       // produces overflow in OCRB1
            break;
            
            default:
        #ifdef _DEBUG_
                print("Unknown command: ");
                putch(receivedChar);
                putch('\n');
        #endif // _DEBUG_
            break;           // always need a statement after a label
        }
        previousChar = receivedChar;
    }
}
