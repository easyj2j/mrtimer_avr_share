/*
 * InternalTimer.cpp
 *
 * Created: 10/15/2014 11:12:30 AM
 *  Author: John
 */

#include "InternalTimer.h"

namespace Atmega328p
{
    /**
     * timerNumber 0: LED/SPK turn-off timer (pulse width)
     * timerNumber 1: LED/SPK turn-on timer
     * timerNumber 2: display counter - never turns off
     * both 8-bit
     */
    InternalTimer::InternalTimer(uint8_t number) : timerNumber(number)
    {
        if(timerNumber > 2) { return; } // throw
            
        switch(timerNumber)
        {
            case 0:
                pTCCRnA = &TCCR0A;
                pTCCRnB = &TCCR0B;
                pTIMSKn = &TIMSK0;
                pTIFRn  = &TIFR0;
                
                pTCNTn  = &TCNT0;
                pOCRnA  = &OCR0A;
                pOCRnB  = &OCR0B;
            break;
            case 2:
                pTCCRnA = &TCCR2A;
                pTCCRnB = &TCCR2B;
                pTIMSKn = &TIMSK2;
                pTIFRn  = &TIFR2;
                
                pTCNTn  = &TCNT2;
                pOCRnA  = &OCR2A;
                pOCRnB  = &OCR2B;
            break;
            case 1: // 16-bit
                pTCCRnA = &TCCR1A;
                pTCCRnB = &TCCR1B;
                //pTCCRnC = &TCCR1C;
                pTIMSKn = &TIMSK1;
                pTIFRn  = &TIFR1;
                
                pTCNT1 = &TCNT1;
                pOCR1A = &OCR1A;
                //pOCR1B = &OCR1B;
            break;
        }
    }
    
    // timer0: normal mode using 1024 prescaler
    // timer1: CTC mode using 1024 prescaler
    // timer2: normal mode using 8 prescaler
    void InternalTimer::Start()
    {
        switch(timerNumber)
        {
            case 0:
                *pTCCRnB = _BV(CS02) | _BV(CS00);
            break;
            case 1:
                *pTCCRnB = _BV(WGM12) | _BV(CS12) | _BV(CS10);
            break;
            case 2:
                *pTCCRnB = _BV(CS21);   // divide clock by 8
                *pTCCRnA = _BV(WGM21);  // compareA
            break;
        }
    }
    
    void InternalTimer::Stop()
    {
        *pTCCRnB = 0;
    }
    
    void InternalTimer::Reset()
    {
        (1 == timerNumber) ? (*pTCNT1 = 0) : (*pTCNTn = 0);
    }
    
    // If reading 8-bit timer, returned high byte = 0.
    uint16_t InternalTimer::Read()
    {
        return (1 == timerNumber) ? *pTCNT1 : *pTCNTn;
    }
    
    void InternalTimer::EnableInterrupt(InterruptState state)
    {
        *pTIMSKn = _BV(static_cast<uint8_t>(state));
    }
    
    void InternalTimer::SetOutputCompareAValue(uint8_t value)
    {
        *pOCRnA = value;
    }
    
    void InternalTimer::SetOutputCompareAValue(uint16_t value)
    {
        *pOCR1A = value;
    }
    
    void InternalTimer::ClearOutputCompareAMatchFlag()
    {
        *pTIFRn |= _BV(OCF1A);
    }
}