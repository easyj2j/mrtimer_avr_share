/*
 * SerialPort.cpp
 *
 * Created: 10/17/2014 9:28:32 AM
 *  Author: John
 */

#include "SerialPort.h"

namespace Atmega328p
{
    SerialPort::SerialPort()
    {
        // Enable pullup R on RX pin to suppress line noise (from atmegaboot loader code)
        DDRD  &= ~_BV(DDD0);
        PORTD |=  _BV(PORTD0);

        // Use default mode (async), parity (none), stop bits (1), & char size (8 bit).
        //UBRR0H = ( (F_CPU/(16*BAUD) - 1) >> 8 );  // UBBR0 = 25 -> H reg not needed
        UBRR0L = (uint8_t)(F_CPU/(16*BAUD) - 1);   // for desired baud rate
    }
    
    void SerialPort::Enable()
    {
        // Enable receiver & interrupt, and transmitter on PORTD
        UCSR0B |= (_BV(RXEN0) | _BV(RXCIE0) | _BV(TXEN0));
    }
    
    void SerialPort::Disable()
    {
        // Enable receiver & interrupt, and transmitter on PORTD
        UCSR0B &= ~(_BV(RXEN0) | _BV(RXCIE0) | _BV(TXEN0));
    }
    
    char SerialPort::Read()
    {
        return UDR0;
    }
    
    void SerialPort::Write(char someChar)
    {
        do {} while( !(UCSR0A & _BV(UDRE0)) );
        UDR0 = someChar;
    }
}