/*
 * Eeprom.cpp
 *
 * Created: 10/17/2014 12:55:41 PM
 *  Author: John
 */ 

#include "Eeprom.h"

namespace Atmega328p
{
    Eeprom::Eeprom() {}
        
    void Eeprom::Update(uint16_t address, const uint8_t data)
    {
        uint8_t currentValue = 0;
        EEAR = address;

        // Wait for previous write to complete.
        while(EECR & _BV(EEPE));

        // Read current value.
        EECR |= _BV(EERE);
        currentValue = EEDR;

        // Write new value.
        if(data != currentValue)
        {
            EEDR = data;

            // Disable interrupts.
            cli();

            EECR  = _BV(EEMPE);
            EECR |= _BV(EEPE);

            // Renable interrupts.
            sei();
        }
    }
    
    uint8_t Eeprom::Read(uint16_t address)
    {
        // Wait for previous write to complete.
        while(EECR & _BV(EEPE));

        EEAR = address;

        EECR |= _BV(EERE);

        return EEDR;
    }
}