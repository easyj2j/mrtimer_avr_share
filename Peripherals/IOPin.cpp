/*
 * IOPin.cpp
 *
 * Created: 10/12/2014 9:53:49 AM
 *  Author: John
 */

#include "IOPin.h"
        
namespace Atmega328p
{
    // ex: IOPin(&PORTC, PORTC2)
    IOPin::IOPin(volatile uint8_t* pPort, uint8_t pin) : 
        pPORT(pPort), pDDR(pPort - 1), pPIN(pPort - 2), THE_PIN(pin) 
    {}
    
    // Hi-Z
    void IOPin::SetInputLow()
    {
        *pDDR  &= ~_BV(THE_PIN);
        *pPORT &= ~_BV(THE_PIN);
    }
    
    // Activate pullup
    void IOPin::SetInputHigh()
    {
        *pDDR  &= ~_BV(THE_PIN);
        *pPORT |= _BV(THE_PIN);
    }
    
    void IOPin::SetOutputLow()
    {
        *pDDR  |= _BV(THE_PIN);
        *pPORT &= ~_BV(THE_PIN);
    }
            
    void IOPin::SetOutputHigh()
    {
        *pDDR  |= _BV(THE_PIN);
        *pPORT |= _BV(THE_PIN);
    }
        
    void IOPin::SetHigh()
    {
       *pPORT |= _BV(THE_PIN);
    }
        
    void IOPin::SetLow()
    {
        *pPORT &= ~_BV(THE_PIN);
    }
        
    void IOPin::TogglePin()
    {
        *pPIN |= _BV(THE_PIN);
    }
        
    bool IOPin::IsHigh()
    {
        return *pPIN & _BV(THE_PIN);
    }

/*
    IOPin::IOPin(volatile uint8_t& rPort, uint8_t pin) :
        rPORT(*(&rPort)), rDDR(*(&rPort - 1)), rPIN(*(&rPort - 2)), THE_PIN(pin)
    {}
    
    void IOPin::SetToOutput(bool isOutput)
    {
        isOutput ? (rDDR |= _BV(THE_PIN)) : (rDDR &= ~_BV(THE_PIN));
    }
    
    void IOPin::SetHigh(bool turnOn)
    {
        turnOn ? (rPORT |= _BV(THE_PIN)) : (rPORT &= ~_BV(THE_PIN));
    }
*/
}
