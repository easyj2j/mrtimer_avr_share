/*
 * ExternalInterrupt.cpp
 *
 * Created: 10/16/2014 4:43:20 PM
 *  Author: John
 */

#include "ExternalInterrupt.h"

namespace Atmega328p
{
    ExternalInterrupt::ExternalInterrupt(uint8_t intNumber, InterruptType type) : intNumber(intNumber)
    {
        SetType(type);
    }
    
    void ExternalInterrupt::SetType(InterruptType type)
    {
        if(0 == intNumber)
        {
            EICRA |= static_cast<uint8_t>(type);
        }
        else   // int1
        {
            EICRA |= static_cast<uint8_t>(type) << 2;
        }
    }
    
    void ExternalInterrupt::Enable()
    {
        EIMSK |= _BV(intNumber);
    }
    
    void ExternalInterrupt::Disable()
    {
        EIMSK &= ~_BV(intNumber);
    }
    
    void ExternalInterrupt::Clear()
    {
        EIFR |= _BV(intNumber);
    }
}
