/*
 * PinChangeInterrupt.cpp
 *
 * Created: 10/16/2014 4:43:20 PM
 *  Author: John
 */

#include "PinChangeInterrupt.h"

namespace Atmega328p
{
    // 0 =< intNumber <= 23, excluding 15
    PinChangeInterrupt::PinChangeInterrupt(uint8_t intNumber) : intNumber(intNumber)
    {
        if(intNumber <=  7)
        { 
            controlRegister = 0; maskRegister = &PCMSK0;
        }
        else if( (8 <= intNumber) && (intNumber <= 14) )
        { 
            controlRegister = 1; maskRegister = &PCMSK1;
        }
        else if( (16 <= intNumber) && (intNumber <= 23) )
        {
            controlRegister = 2; maskRegister = &PCMSK2;
        }
        else { return; }    // error
    }
    
    void PinChangeInterrupt::Enable()
    {
        PCICR |= _BV(controlRegister);
        *maskRegister |= _BV(intNumber % 8);
    }
    
    void PinChangeInterrupt::Disable()
    {
        *maskRegister &= ~_BV(intNumber % 8);
        // If all interrupts in maskRegister are disabled -> disable controlRegister. [not really necessary]
        if(0 == maskRegister) { PCICR &= ~_BV(controlRegister); }
    }
    
    void PinChangeInterrupt::Clear()
    {
        PCIFR |= _BV(controlRegister);
    }
}
