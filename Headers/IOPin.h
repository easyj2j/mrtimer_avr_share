/*
 * IOPin.h
 *
 * Created: 10/12/2014 9:53:35 AM
 *  Author: John
 */ 

#ifndef _ATMEGA328P_IOPIN_H_
#define _ATMEGA328P_IOPIN_H_

#include <avr/io.h>
        
namespace Atmega328p
{
    class IOPin
    {
    public:
        // ex: IOPin(&PORTC, PORTC2)
        IOPin(volatile uint8_t* pPort, uint8_t pin);
        
        void SetInputLow();         // hi-z
        // Activate pullup.
        // Use for unconnected pins.
        // Deactivate by setting direction -> output or pin -> low.
        void SetInputHigh();     
        void SetOutputLow();
        void SetOutputHigh();
        void SetHigh();
        void SetLow();
        void TogglePin();
        
        bool IsHigh();
        
    private:
        volatile uint8_t* const pPORT;
        volatile uint8_t* const pDDR;
        volatile uint8_t* const pPIN;
        const uint8_t THE_PIN;
    };
}

#endif /* _ATMEGA328P_IOPIN_H_ */

//LED:
//volatile uint8_t* const pPORT = &PORTC;
//volatile uint8_t* const pDDR  = &DDRC;
//volatile uint8_t* const pPIN  = &PINC;
//static const uint8_t LED_PIN = PORTC2;   // ATmega328 pin 25
    
/*
    class IOPin
    {
        public:
            IOPin(volatile uint8_t& rPort, uint8_t pin);
            void SetToOutput(bool isOutput);
            void SetHigh(bool turnOn);

        private:
            volatile uint8_t& rPORT;
            volatile uint8_t& rDDR;
            volatile uint8_t& rPIN;
            const uint8_t THE_PIN;
    };
*/
