/*
 * PinChangeInterrupt.h
 *
 * Created: 10/16/2014 4:43:08 PM
 *  Author: John
 */ 


#ifndef _ATMEGA328P_PINCHANGEINTERRUPT_H_
#define _ATMEGA328P_PINCHANGEINTERRUPT_H_

#include <avr/io.h>

namespace Atmega328p
{
    class PinChangeInterrupt
    {
    public:
        // 0 =< intNumber <= 23, excluding 15
        PinChangeInterrupt(uint8_t intNumber);
        void Enable();
        void Disable();
        void Clear();
        
    private:
        const uint8_t intNumber;
        uint8_t controlRegister;
        volatile uint8_t* maskRegister;
    };
}

#endif /* _ATMEGA328P_PINCHANGEINTERRUPT_H_ */