/*
 * Eeprom.h
 *
 * Created: 10/17/2014 12:55:30 PM
 *  Author: John
 */ 


#ifndef _ATMEGA328P_EEPROM_H_
#define _ATMEGA328P_EEPROM_H_

#include <avr/interrupt.h>

namespace Atmega328p
{
    class Eeprom
    {
    public:
        Eeprom();
        void Update(uint16_t address, const uint8_t data);
        uint8_t Read(uint16_t address);
    };
}

#endif /* _ATMEGA328P_EEPROM_H_ */