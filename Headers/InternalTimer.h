/*
 * InternalTimer.h
 *
 * Created: 10/15/2014 11:12:18 AM
 *  Author: John
 */ 

#ifndef _ATMEGA328P_INTERNALTIMER_H_
#define _ATMEGA328P_INTERNALTIMER_H_

#include <avr/io.h>

namespace Atmega328p
{    
    enum class InterruptState
    {
        OVERFLOW,
        COMPARE_MATCH_A,
        COMPARE_MATCH_B
    };
    
    class InternalTimer
    {
    public:
        /**
         * timerNumber 0: LED/SPK turn-off timer (pulse width)
         * timerNumber 1: LED/SPK turn-on timer
         * timerNumber 2: display counter - never turns off
         */
        InternalTimer(uint8_t number);
        void Start();
        void Stop();
        // If reading 8-bit timer, returned high byte = 0.
        uint16_t Read();
        void Reset();
        void EnableInterrupt(InterruptState state);
        void SetOutputCompareAValue(uint8_t value);
        void SetOutputCompareAValue(uint16_t value);
        void ClearOutputCompareAMatchFlag();
    private:
        uint8_t timerNumber;
        volatile uint8_t* pTCCRnA;
        volatile uint8_t* pTCCRnB;
        volatile uint8_t* pTCNTn;
        volatile uint8_t* pOCRnA;
        volatile uint8_t* pOCRnB;
        volatile uint8_t* pTIMSKn;
        volatile uint8_t* pTIFRn;
        
        // 16-bit timer1
        //volatile uint8_t* pTCCR1C;
        volatile uint16_t* pTCNT1;
        volatile uint16_t* pOCR1A;
        //volatile uint16_t* pOCR1B;
    };
}

#endif /* _ATMEGA328P_INTERNALTIMER_H_ */