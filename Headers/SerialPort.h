/*
 * SerialPort.h
 *
 * Created: 10/17/2014 9:28:18 AM
 *  Author: John
 */ 

#ifndef _ATMEGA328P_SERIALPORT_H_
#define _ATMEGA328P_SERIALPORT_H_

#include <avr/io.h>

namespace Atmega328p
{
    class SerialPort
    {
    public:
        SerialPort();
        void Enable();
        void Disable();
        char Read();
        void Write(char someChar);
    private:
        // No memory space required - optimized away. 
        static const uint32_t F_CPU = 8000000UL;
        static const uint32_t BAUD = 38400UL;
    };
}

#endif /* _ATMEGA328P_SERIALPORT_H_ */