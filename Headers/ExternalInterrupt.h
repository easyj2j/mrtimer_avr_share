/*
 * ExternalInterrupt.h
 *
 * Created: 10/16/2014 4:43:08 PM
 *  Author: John
 */ 


#ifndef _ATMEGA328P_EXTERNALINTERRUPT_H_
#define _ATMEGA328P_EXTERNALINTERRUPT_H_

#include <avr/io.h>

namespace Atmega328p
{
    enum class InterruptType
    {
        LOW_LEVEL,
        LOGIC_CHANGE,
        FALLING_EDGE,
        RISING_EDGE
    };
    
    class ExternalInterrupt
    {
    public:
        ExternalInterrupt(uint8_t intNumber, InterruptType type);
        void SetType(InterruptType type);
        void Enable();
        void Disable();
        void Clear();
        
    private:
        const uint8_t intNumber;
    };
}

#endif /* _ATMEGA328P_EXTERNALINTERRUPT_H_ */