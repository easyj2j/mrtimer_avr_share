/**
 * MRTimer.h
 * Use with MRTimer.cpp
 *
 * v3.3: Uses external clock (crystal) instead of internal rc clock.
 *
 * Created: 1/28/2015
 * Author: J Mac
 */

#ifndef MOTION_REACTION_TIMER_H_
#define MOTION_REACTION_TIMER_H_

#pragma region includes
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "Eeprom.h"
#include "ExternalInterrupt.h"
#include "InternalTimer.h"
#include "IOPin.h"
#include "PinChangeInterrupt.h"
#include "SerialPort.h"
#pragma endregion

typedef char byte;

//#define _DEBUG_     // print debug info

// TODO: replace w C++ style cast operator
#define REG_CAST volatile uint8_t*

namespace MRTimer
{
    using namespace Atmega328p;
    
    static const char* const FIRMWARE_VERSION = "3.3";

    class Timer
    {
    public:
        Timer();
        void Start();
        
        /** 
         * IS_BASIC_MODEL sets the functionality of the timer between two different models:
         *   true: Standalone timer [BASIC_MODEL]
         *         - no Bluetooth or speaker.
         *         - displays response time or motion time, reaction time unavailable.
         *         - motionSwitch clears display on HI -> LO transition (external interrupt 0).
         *         - two different modes determined by state of motionSwitch at start up:
         *              LO: BasicMotion - displays motion time.
         *                  - motionSwitch starts timer/display on LO -> HI transition.
         *              HI: BasicResponse - displays response time.
         *                  - motionSwitch not used or needed (default start up state is HI).
         *
         *   false: Standalone timer with Bluetooth [DELUXE_MODEL]
         *         - communicates with phone app/pc over BT.
         *         - external interrupt 0 (motionSwitch) provides time mark separating reaction & motion times.
         *         - displays response time, stores reaction time, with motion time calculated on app.
         *
         * Response time = Reaction time + Motion time
         */
        static const bool IS_BASIC_MODEL = false; // slightly increases code size over using #define
        
        /*
         * When in BASIC_MODE, isBasicResponse determines whether the timer is used as a 
         *  true:  Response timer, or
         *  false: Motion timer
         */
        static bool isBasicResponse;
        
        // Flags set by the ISRs - must be public static.
        static bool interrupt0, interrupt1, pcint1, timer0_ovf, timer1_compa, timer2_compa, timer2_ovf, usart_rx;
        // ISR(PCINT1_vect)
        static IOPin competitionTrigger;
        // ISR(TIMER1_COMPA_vect)
        static InternalTimer triggerDelayTimer; // 16-bit timer
        // ISR(INT1_vect)
        static ExternalInterrupt stopSwitchInterrupt;

    private:
        static const uint8_t NUMBER_OF_DIGITS = 4;
        static const volatile uint8_t* DIGIT_PORTS[NUMBER_OF_DIGITS]; 
        // = {PORTC,PORTD,PORTD,PORTD} = {0x28,0x2B,0x2B,0x2B}
                
        static const uint16_t DISPLAYTIMER_FREQ = 0x1E84;
        static const uint8_t DISPLAYTIMER_MAXCOUNT = 0xFF;
        static const uint8_t TRIGGER_MIN_DELAY = 1;
        static const uint8_t TRIGGER_MAX_DELAY = 8;
        static const uint8_t EEPROM_ADDR_DELAY_MIN = 0x02;
        static const uint8_t EEPROM_ADDR_DELAY_MAX = 0x03;
        static const char ZERO_APP_DISPLAY = '!';   // sent to Android app (see MotionSwitch_psr, OutputTime)
        
        /*============= IO PIN definitions ==================================*/
        
        /***** 7 SEGMENT DISPLAY definitions *****/
        /**
         * Quad 7 Segment display, common-cathode.
         * All segment pins on same port.
         * PORTB bits = f,a,e,d,  dp,c,g,b
         */
        // Segments to turn on corresponding to numbers 0-9 without a decimal point.
        static const uint8_t SEVEN_SEGMENT[10];
        // = {0x7D,0x05,0x73,0x57,0x0F,0x5E,0x7E,0x45,0x7F,0x4F}
        
        // segmentsPort
        struct PortRegisters
        {
            PortRegisters(volatile uint8_t* pPORTx, volatile uint8_t* pDDRx, volatile uint8_t* pPINx) :
            pPORT(pPORTx), pDDR(pDDRx), pPIN(pPINx) {}
            volatile uint8_t* const pPORT;
            volatile uint8_t* const pDDR;
            volatile uint8_t* const pPIN;
        }
        segmentsPort{&PORTB, &DDRB, &PINB}; // this is correct syntax
        /*
           Bug in VS2010/AS6: if the segmentsPort definition/initialization is placed on the same line as the 
           struct end '}', the struct outlining collapses the rest of the file (it's caused by the '{' after segmentsPort).
           Probably due to incomplete/incorrect implementation of C++11.
        */
        
        static const uint8_t DIGITS[NUMBER_OF_DIGITS];    
        // = {PORTC0,PORTD5,PORTD6,PORTD7}, LSD -> MSD
            
        IOPin decimalPoint{&PORTC, PORTC4};             // ATmega328 pin 27
        IOPin segmentA{&PORTD, PORTD4};                 // ATmega328 pin  6
        /*****************************************/

        /***** SWITCH definitions ****************/
        // Motion & Stop switches
        volatile uint8_t* const pSwitchPort = &PORTD;
        /**
         * Ready/Start/Motion Switch
         * Internally pulled HI
         *
         * In DELUXE_MODEL:
         *  HI -> LOW -> clear display & restart random delay
         *  LOW -> HI -> record reaction time
         *
         * In BASIC_MODEL, during startup:
         *   HI (default)          -> isBasicResponse == true (BasicResponse)
         *   LO (switch depressed) -> isBasicResponse == false (BasicMotion)
         * In BASIC_MODEL, after startup:
         *   BasicResponse: motionSwitch not used
         *   BasicMotion: 
         *     HI -> LO -> clear display
         *     LO -> HI -> start timer
         */
        IOPin motionSwitch{&PORTD, PORTD2};             // ATmega328 pin 4
        /**
         * Stop Switch
         * Internally pulled HI
         * HI -> LOW -> trigger INT1 -> latch display, record response time
         */
        IOPin stopSwitch{&PORTD, PORTD3};               // ATmega328 pin 5
        /*****************************************/
        
        /***** TRIGGER definitions ***************/
        /**
         * Trigger pulsed LOW -> HI (on) -> LOW
         *
         * On after LED_ON_TIME timer counts (display starts simultaneously).
         * Off after LED_OFF_TIME timer counts.
         */
        IOPin triggerLed{&PORTC, PORTC1};               // ATmega328 pin 24
        IOPin triggerBzr{&PORTC, PORTC3};               // ATmega328 pin 26
            
        enum class Trigger : uint8_t
        {
            LED = 1, BUZZER, BOTH   // test BOTH to see if it causes the LED to dim greatly
        } 
        trigger = Trigger::LED;
        /*****************************************/
        
        enum class CompetitionMode : uint8_t
        {
            OFF = 1, MASTER, SLAVE
        } 
        compMode = CompetitionMode::OFF;
            
        /*===================================================================*/
        
        // Store up to RT_MAX_ENTRIES scores in circular buffer.
        static const uint8_t RT_MAX_ENTRIES = 10;
        // response time = reaction time + motion time
        struct ResponseTime
        {
            uint16_t reaction;
            uint16_t response;
        } responseTimes[RT_MAX_ENTRIES];
        uint8_t rtIndex;    // incremented just after storing score in StopSwitch_psr()

        InternalTimer triggerPulseTimer{0};
        InternalTimer displayTimer{2};
        ExternalInterrupt motionSwitchInterrupt{0, InterruptType::LOGIC_CHANGE};
        PinChangeInterrupt competitionInterrupt{PCINT12};
        Eeprom eeprom{};
        SerialPort serialPort{};
        char receivedChar, previousChar;  // stores prev command to test for preludes
        
        // Defines random delay range for triggerDelayTimer.
        uint8_t delayMin, delayMax;
        // Used to divide triggerPulseTimer freq by 2
        bool timer0DividedBy2;
        
        // Loop counters for timer2 ISR (display counter)
        uint8_t digit0Counter, digit1Counter, digit2Counter, digit3Counter;
        // Latch display on true, count on false
        bool latch;
        // Millisecond counter; incremented by DisplayCounter_psr
        uint16_t msCounter;
                
        /**
         * Determines what action to take when Motion Switch is released.
         *
         * Set to true in TriggerStart_psr (start display counting).
         * Tested in MotionSwitch_psr & StopSwitch_psr.
         *  true: store reaction time
         *  false: do nothing
         * Set to false in StopSwitch_psr.
         */
        bool timingState;
        
        /**
         * When set, indicates ZERO_APP_DISPLAY was sent to phone app
         *  notifying it to zero its display.
         *
         * Set to true in MotionSwitch_psr (motion switch pressed)
         *  only when timingState == false && outputTime == true.
         * Set to false in OutputTime().
         */
        bool wasAppNotified;

        // Set in USART_RX_psr
        bool printTime;
        bool outputTime;
                
#pragma region prototypes
        /**
         * When an interrupt occurs the corresponding interrupt service routine, ISR(X_vect), is called.
         * The ISR simply sets the corresponding bool flag.
         * Polling over these flags results in the corresponding method listed below being called and the flag reset.
         */
        void MotionSwitch_psr();
        void MotionSwitch_Basic_psr();
        void CompModeSlaveTrigger_psr();
        void TriggerStart_psr();
        void TriggerStop_psr();
        void StopSwitch_psr();
        void DisplayCounter_psr();
        void USART_RX_psr();
        
        void InitializeMCU();
        void SetCompetitionMode(CompetitionMode mode);
        void SetDelayFromEeprom();
        uint16_t LedOnTime(byte delayMin, byte delayMax);

        void LedSpk_TurnOnOff(bool turnOn);
        void SendOutputToSpeaker();

        void putch(char ch);
        void putnum(uint16_t num);
        void printnumln(uint16_t num);
        void print(const char* str);
        void println(const char* str);
        void OutputFirmwareVersion();
        void PrintTime();
        void PrintTimes();
        void OutputTime();
        void PrintHeader();
#pragma endregion
    };
}
#endif /* MOTION_REACTION_TIMER_H_ */